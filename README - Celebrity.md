HW5 - Task 1
===

Kevin Yee (kyee4), John Wilson (jwils140), Michael Ashmead (mashmea1)

In order to determine the celebrity by only asking whether person a knew person b, we first parsed the text file a created a directional graph where edges traveled from each person to the people they knew. Then, beginning with person 0, we sequentially asked if they knew the next person. If they did know the person, then that person became the new person of interest because the person that knew them could not be the celebrity. Eventually, whoever the person of interest was once all of the people in the graph had been checked was the celebrity.
