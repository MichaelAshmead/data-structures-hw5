HW5 - Task 3
===

Kevin Yee (kyee4), John Wilson (jwils140), Michael Ashmead (mashmea1)

Our strategy for this task was to add each course into a directional graph with edges that travel from courses to their prerequisites. Then we iterated through the graph and kept track of every leaf vertex (which correspond to courses that do not have any existing prerequisites) and then deleted each vertex in that list and added them to the current semester's schedule. Because courses were removed from the graph each semester as they were taken, new verticies became leaves, allowing them to be taken the subsequent semester, and so on until all courses had been taken.
