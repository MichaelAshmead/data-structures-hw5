import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;


/** A class to find the celebrity at a party.
 * @author Kevin Yee (kyee4), John Wilson (jwils140), Michael Ashmead (mashmea1)
 */
public class Celebrity {
    
    /** A direction graph to hold party relationships. */
    private Digraph<Integer> graph;
    /** Number of people at the party. */
    private int numPeople;
    /** The index of the celebrity. */
    private int celebIndex;

    /** Construct a Celebrity from a Scanner loaded with 
     *  the input relationships.
     * @param input the input Scanner
     */
    public Celebrity(Scanner input) {
        this.graph = new Digraph<Integer>();

        this.numPeople = input.nextInt();
        
        while (input.hasNext()) {
            this.graph.addEdge(input.nextInt(), input.nextInt());
        }
        
        this.celebIndex = this.findCeleb();
    }
    
    /** Find the index of the celebrity. 
     * @return the index of the celebrity
     */
    private int findCeleb() {
        int currPerson = 0;
        for (int i = currPerson + 1; i < this.numPeople; i++) {
            if (this.graph.checkEdge(currPerson, i)) {
                currPerson = i;
            } 
        }
        return currPerson;
    }
    
    /** Convert the Celebrity to a String.
     * @return the String
     */
    public String toString() {
        String output = "";
//        output += this.graph;
        output += "The celebrity is " + this.celebIndex;
        return output;
    }
    
    /** Celebrity class driver.
     * @param args the zeroth index is the input file name
     */
    public static void main(String[] args) {
        System.out.println("Celebrity.java");

        Celebrity celeb;
        
        try {
            Scanner s = new Scanner(new FileReader(args[0]));
            celeb = new Celebrity(s);
            s.close();

            System.out.println(celeb);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
                
    }

}
