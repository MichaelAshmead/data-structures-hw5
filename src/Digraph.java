import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/** Direction Graph Class.
 * @author Kevin Yee (kyee4), John Wilson (jwils140), Michael Ashmead (mashmea1)
 *
 * @param <V> the type of the direction graph
 */
public class Digraph<V> {
    
    /** HashMap to store the vertices and edges. */
    private HashMap<V, HashSet<V>> graph;

    /** Digraph Constructor. */
    public Digraph() {
        this.graph = new HashMap<V, HashSet<V>>();
    }
    
    /** Add a vertex to the graph.
     * @param data the data of the vertex to be added
     */
    public void addVertex(V data) {
        if (!this.graph.containsKey(data)) {
            this.graph.put(data, new HashSet<V>());
        }
    }
    
    /** Add an edge from first to second. Create the vertices if necessary.
     * @param first the vertex origin
     * @param second the vertex first is pointed to 
     */
    public void addEdge(V first, V second) {
        this.addVertex(first);
        this.addVertex(second);
        
        this.graph.get(first).add(second);
    }
    
    /** Check if there is an edge from first to second.
     * @param first the data of the origin of the edge
     * @param second the data of the end of the edge
     * @return true if edge exists, otherwise false
     */
    public boolean checkEdge(V first, V second) {
        return this.graph.get(first).contains(second);
    }
    
    /** Check if node exists. 
     * @param data the data of the node to be checked
     * @return true if node exists, otherwise false
     */
    public boolean checkIfNode(V data) {
        return this.graph.containsKey(data);
    }
    
    /** Check if node is a leaf.
     * @param data the data of the node to be checked
     * @return true if node is a leaf, otherwise false
     */
    public boolean checkIfLeafNode(V data) {
        return this.graph.containsKey(data) && this.graph.get(data).size() == 0;
    }
    
    /** Remove a node from the graph and all edges that point to that node.
     * @param data the data of the node to be removed
     */
    public void removeNode(V data) {
        if (this.checkIfNode(data)) {
            this.graph.remove(data);
            for (V currKey : this.graph.keySet()) {
                if (this.graph.get(currKey).contains(data)) {
                    this.graph.get(currKey).remove(data);
                }
            }
        }  
    }
    
    /** Get the Set of vertex values from the graph. 
     * @return the Set
     */
    public Set<V> getKeySet() {
        return this.graph.keySet();
    }
    
    /** Convert the graph to a String.
     * @return the String
     */
    public String toString() {
        String output = "";
        for (V currKey : this.graph.keySet()) {
            output += currKey + "\t" + this.graph.get(currKey) + "\n";
        }
        return output;
    }
    
}