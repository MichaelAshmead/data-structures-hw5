//Michael Ashmead (mashmea1), John Wilson (jwils140), Kevin Yee (kyee4)

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * WordSort class that will sort unsorted words based on a given dictionary.
 */
public final class WordSort {
    
    /**
     * This constructor is not called. It is only here to get rid of
     * CheckStyle errors.
     */
    private WordSort() {
        //prevent instantiation
    }
    
    /**
     * @param args
     *        The first argument is the dictionary filename.
     *        The second argument is the unsorted filename.
     *        The third argument is the name of the to-be-created sorted file.
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        ArrayList<String> dictionary = readInput(args[0]);
        
        ArrayList<Character> alphabet = generateAlphabet(dictionary);

        ArrayList<String> unsorted = readInput(args[1]);
        
        ArrayList<String> sorted = sortWords(unsorted, alphabet);
        
        writeToFile(sorted, args[2]);
    }

    /**
     * @param filename that this method is to read.
     * @return ArrayList<String> of words that were in the file.
     * @throws IOException 
     */
    private static ArrayList<String>
        readInput(String filename) throws IOException {
        ArrayList<String> list = new ArrayList<String>();
        
        File file = new File(filename);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        while (line != null) {
            String[] temp = line.trim().split("\\s+");
            for (int x = 0; x < temp.length; x++) {
                list.add(temp[x]);
            }
            line = br.readLine();
        }
        br.close();
        
        return list;
    }
    
    /**
     * @param words to generate an alphabet from.
     * @return ArrayList<Character>
     */
    private static ArrayList<Character>
        generateAlphabet(ArrayList<String> words) {
        //create alphabet based on dictionary
        Digraph<Character> graph = new Digraph<Character>();
        for (int x = 1; x < words.size(); x++) {
            //compare current word with previous word
            int pointer = 0;
            char char1 = words.get(x).charAt(pointer);
            char char2 = words.get(x - 1).charAt(pointer);
            while (char1 == char2) {
                pointer++;
                char1 = words.get(x).charAt(pointer);
                char2 = words.get(x - 1).charAt(pointer);
            }
            graph.addEdge(char1, char2);
        }
        
        //put all distinct characters into unordered ArrayList
        ArrayList<Character> unordered = new ArrayList<Character>();
        for (int x = 0; x < words.size(); x++) {
            for (int y = 0; y < words.get(x).length(); y++) {
                if (!unordered.contains(words.get(x).charAt(y))) {
                    unordered.add(words.get(x).charAt(y));
                }
            }
        }


        //keep removing nodes that don't point to anything and place
        //in ArrayList and remove from unordered ArrayList and graph
        ArrayList<Character> alphabet = new ArrayList<Character>();
        while (unordered.size() > 0) {
            for (int x = 0; x < unordered.size(); x++) {
                if (graph.checkIfLeafNode(unordered.get(x))) {
                    alphabet.add(unordered.get(x));
                    graph.removeNode((char) unordered.get(x));
                    unordered.remove(x);
                }
            }
        }

        return alphabet;
    }
    
    /**
     * @param words to be sorted
     * @param alphabet to define how words will be sorted
     * @return ArrayList<String>
     */
    private static ArrayList<String> sortWords(ArrayList<String> words,
            ArrayList<Character> alphabet) {
        BST tree = new BST(alphabet);
        for (String currentWord : words) {
            tree.insert(currentWord);
        }
        return tree.inOrder();
    }
    
    /**
     * @param words to be written to file.
     * @param filename to name new file of sorted words.
     * @throws FileNotFoundException 
     */
    private static void writeToFile(ArrayList<String> words,
            String filename) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(filename);
        for (String currentWord : words) {
            writer.println(currentWord);
        }
        writer.close();
    }
}
