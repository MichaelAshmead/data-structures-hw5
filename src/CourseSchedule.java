import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Scanner;

/** A class to calculate the most efficient
 *  way to fulfill course requirements.
 * @author Kevin Yee (kyee4), John Wilson (jwils140), Michael Ashmead (mashmea1)
 */
public class CourseSchedule {
    
    /** Direction graph to hold the courses. */
    private Digraph<String> courses;
    /** The course schedule divided into semesters. */
    private String schedule;

    /** Construct CourseSchedule from Scanner loaded
     *  with input data.
     * @param input the input Scanner
     */
    public CourseSchedule(Scanner input) {
        this.courses = new Digraph<String>();
               
        String courseName;
        while (input.hasNext()) {
            courseName = input.next();
            this.courses.addVertex(courseName);
            
            Scanner prereqs = new Scanner(input.nextLine());
            while (prereqs.hasNext()) {
                this.courses.addEdge(courseName, prereqs.next());
            }
            prereqs.close();
        }
        
        this.schedule = this.generateSchedule();
    }
    
    /** Generate a String that has the course schedule.
     * @return the String
     */
    private String generateSchedule() {
        String output = "";
        int currSem = 0;
        LinkedList<String> nodesToDelete = new LinkedList<String>();
        while (!this.courses.getKeySet().isEmpty()) {
            output += "Semester " + ++currSem + ":\n";
            for (String currCourse : this.courses.getKeySet()) {
                if (this.courses.checkIfLeafNode(currCourse)) {
                    output += currCourse + "\n";
                    nodesToDelete.add(currCourse);
                }
            }
            
            for (String courseDelete : nodesToDelete) {
                this.courses.removeNode(courseDelete);
            }
            
            nodesToDelete.clear();
        }
        
        output += "Total number of semesters required: " + currSem;
        
        return output;
    }
    
    /** Convert CourseSchedule to String.
     * @return the String
     */
    public String toString() {
        return this.schedule;
    }
    
    /** CourseSchedule driver class.
     * @param args zeroth index is the input file name
     */
    public static void main(String[] args) {
        
        try {
            Scanner s = new Scanner(new FileReader(args[0]));
            CourseSchedule schedule = new CourseSchedule(s);
            s.close();
            System.out.println(schedule);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
