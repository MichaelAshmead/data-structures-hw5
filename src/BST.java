//Michael Ashmead (mashmea1), John Wilson (jwils140), Kevin Yee (kyee4)

import java.util.ArrayList;

/**
 * Binary Search Tree class for sorting unsorted words.
 */
public class BST {
    /**
     * Top node of the tree.
     */
    private Node root;
    
    /**
     * Alphabet that we will sort words by.
     */
    private ArrayList<Character> alphabet = new ArrayList<Character>();
    
    /**
     * Constructor method.
     * @param tempAlphabet that defines how the words will be sorted.
     */
    public BST(ArrayList<Character> tempAlphabet) {
        this.root  = null;
        this.alphabet = tempAlphabet;
    }
    
    /**
     * Node class.
     */
    private class Node {
        /**
         * Key of node.
         */
        private String nkey = null;

        /**
         * Left child node.
         */
        private Node nleft = null;
        
        /**
         * Right child node.
         */
        private Node nright = null;
    }
    
    /**
     * @param theKey we want to insert.
     */
    public void insert(String theKey) {
        Node n = new Node();
        n.nkey = theKey;
        n.nleft = null;
        n.nright = null;
        this.root = this.insertHelper(this.root, n);
    }
    
    /**
     * @param root2 - Current root under inspection.
     * @param n - The Node about to be inserted.
     * @return Node returned.
     */
    private Node insertHelper(Node root2, Node n) {
        if (root2 == null) {
            return n;
        } else if (this.compareTo(n.nkey, root2.nkey) < 0) {
            root2.nleft = this.insertHelper(root2.nleft, n);
        } else {
            root2.nright = this.insertHelper(root2.nright, n);
        }
        return root2;
    }
    
    /**
     * @param key1 first word to compare
     * @param key2 second word to compare
     * @return negative integer if the first string comes before the
     *         second string otherwise return a positive integer
     */
    private int compareTo(String key1, String key2) {
        int pointer = 0;
        while (key1.charAt(pointer) == key2.charAt(pointer)) {
            pointer++;
            if (key1.length() == pointer) {
                return -1;
            } else if (key2.length() == pointer) {
                return 1;
            }
        }
        
        int index1 = this.alphabet.indexOf(key1.charAt(pointer));
        int index2 = this.alphabet.indexOf(key2.charAt(pointer));
        if (index1 < index2) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * @return ArrayList<String> of elements in order.
     */
    public ArrayList<String> inOrder() {
        return this.inOrderHelper(this.root);
    }
    
    /**
     * @param n - The root of the whole tree.
     * @return ArrayList<String> of elements in order.
     */
    private ArrayList<String> inOrderHelper(Node n) {
        ArrayList<String> list = new ArrayList<String>();
        if (n != null) {
            list.addAll(this.inOrderHelper(n.nleft));
            list.add(n.nkey);
            list.addAll(this.inOrderHelper(n.nright));
        }
        return list;
    }
}
