HW5 - Task 2
===

Kevin Yee (kyee4), John Wilson (jwils140), Michael Ashmead (mashmea1)

Generate alphabet based on dictionary by doing the following:
Start with 2nd word and check with previous word. Find first character that is different than in the previous word. Add the previous character node if it doesn't exist to the graph. Now connect previous character with an edge pointing to current character of current word. At the end generate a spanning tree using the graph.

Sort words using a binary tree by doing the following:
First word is the root of the binary tree.
When putting the next word into the tree, compare each letter using previously generated alphabet in part (1) to determine if the word should go into the left subtree or right subtree.
Print out binary tree in-order.
